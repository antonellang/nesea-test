import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/models/category.model';
import { CategoryService } from 'src/app/services/category.service';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public categories: Category[];

  constructor(private _catService: CategoryService, private _cartService: CartService) {
    this.categories = [];
  }

  ngOnInit() {
    this._catService.list().subscribe(c => this.categories = c);
  }

  get count(): number {
    return this._cartService.count;
  }

}
