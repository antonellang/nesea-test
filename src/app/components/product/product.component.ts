import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/models/product.model';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  @Input() public product: Product;

  constructor(private _cartService: CartService) {
  }

  ngOnInit() {
  }

  public addToCart(p: Product) {
    this._cartService.add(p);
  }

  public removeToCart(p: Product) {
    this._cartService.remove(p);
  }

}
