import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/app/models/product.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  public products: Product[];

  constructor(private _prodService: ProductService, private _route: ActivatedRoute) {
    this.products = [];
  }

  ngOnInit() {
    this._route.paramMap.subscribe(params => {
      this._prodService.list(params.get('id')).subscribe(p => this.products = p);
    });
  }

}
