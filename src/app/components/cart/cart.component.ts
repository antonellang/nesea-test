import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product.model';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  public products: Product[];

  constructor(private _cartService: CartService) { 
    this.products = [];
  }

  ngOnInit() {
    this.products = this._cartService.list;
  }
}
