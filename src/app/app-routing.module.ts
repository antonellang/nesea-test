import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './components/products/products.component';
import { CartComponent } from './components/cart/cart.component';

const routes: Routes = [{
  component: ProductsComponent,
  path: 'products'
}, {
  component: ProductsComponent,
  path: 'products/:id'
}, {
  component: CartComponent,
  path: 'cart'
}, {
  path: '',
  pathMatch: 'full',
  redirectTo: 'products'
}];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
