import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { ICategory } from '../interfaces/category.interface';
import { Category } from '../models/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private _http: HttpClient) { }

  public list() {
    return this._http.get<ICategory[]>(`${environment.url}/categories`).pipe(map(
      c => {
        const categories: Category[] = [];

        for (const cat of c){
          categories.push(new Category(cat.id, cat.name));
        }

        return categories;
      }));
  }
}
