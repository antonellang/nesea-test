import { Injectable } from '@angular/core';
import { Product } from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  public products: Product[];

  constructor() {
    this.products = [];
  }

  get count() {
    return this.products.length;
  }

  public add(p: Product): void {
    this.products.push(p);
  }

  public remove(p: Product): void {
    this.products.splice(
      this.products.findIndex(prod => prod.id === p.id), 1
    );
  }

  get list() {
    return this.products;
  }
}
