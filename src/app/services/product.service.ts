import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Product } from '../models/product.model';
import { IProduct } from '../interfaces/product.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private _http: HttpClient) { }

  public list(id: string) {
    let url = `${environment.url}/products`;

    if (id) {
      url += `?category_id=${id}`;
    }

    return this._http.get<IProduct[]>(url).pipe(map(
      p => {
        const products: Product[] = [];

        for (const prod of p){
          products.push(new Product(prod.id, prod.name, prod.description, prod.image, prod.price, prod.categoryId));
        }

        return products;
      }));
  }
}
